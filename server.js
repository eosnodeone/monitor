const express = require('express');
const app = express();
const server = require('http').createServer(app);
const request = require('request');
const async = require('async');
const _ = require('lodash');
const Heartbeat = require('./heartbeat');

// Enable socket.io
const io = require('socket.io')(server);

// 개발 환경에서는 namespace를 사용하지 않는다. (사용할 경우 CORS 에 걸림)
const api = process.env.NODE_ENV === 'production' ? io.of('/api') : io;

app.use(express.static('build'));


var nodes = {
  mainnet: [
    { name: 'nodeone api', url: 'http://api.main-net.eosnodeone.io' },
    { name: 'eosys api', url: 'https://rpc.eosys.io' }
  ],
  telos: []
};

// Production
if (process.env.NODE_ENV == 'production') {
  nodes = {
    mainnet: [
      { name: 'prod01', url: 'http://10.178.166.217:8888' },
      { name: 'api01', url: 'http://172.40.39.132:8888' },
      { name: 'api02', url: 'http://172.40.38.242:8888' },
      // { name: 'seed01', url: 'http://172.40.36.100:8888' },
      { name: 'ghost01', url: 'http://172.40.27.117:8888' }
    ],
    telos: [
      { name: 'telos', url: 'http://172.40.27.117:8887' }
    ],
    kylin: [
      { name: 'kylin', url: 'http://172.40.27.117:8890' }
    ]
  };
}


const skt = {
  platforms: [
    {
      name: 'EOS Mainnet',
      key: 'mainnet'
    },
    {
      name: 'Telos testnet Stage 2',
      key: 'telos'
    },
    {
      name: 'CryptoKylin Testnet',
      key: 'kylin'
    }
  ],
  applications: {
    mainnet: [],
    telos: [],
    kylin: []
  },
  nodes: {
    mainnet: [],
    telos: [],
    kylin: []
  }
};


// Start heartbeating

var heartbeats = {};

function run() {
  nodes.mainnet.forEach((node) => {
    var hb = new Heartbeat({key: node.name, url: node.url + '/v1/chain/get_info'});
    hb.start();
    heartbeats[node.name] = hb;
  });

  nodes.telos.forEach((node) => {
    var hb = new Heartbeat({key: node.name, url: node.url + '/v1/chain/get_info'});
    hb.start();
    heartbeats[node.name] = hb;
  }); 

  nodes.kylin.forEach((node) => {
    var hb = new Heartbeat({key: node.name, url: node.url + '/v1/chain/get_info'});
    hb.start();
    heartbeats[node.name] = hb;
  });  
}

run();



// connection event handler
// connection이 수립되면 event handler function의 인자로 socket인 들어온다
api.on('connection', function(socket) {

  socket.on('request data', function(data) {

    // console.log('received request data');

    async.parallel({
      mainnet_nodes: function(callback) {
        // console.log('mainnet_nodes');
        var r = [];
        async.each(nodes.mainnet, function(node, cb) {
          var hb = heartbeats[node.name];
          var data = hb.data()
          r.push({
            data,
            name: node.name,
            response_time: hb.resTime()
          });
          cb();
        }, function(err) {
          // console.log('mainnet_nodes end');
          callback(err, r);
        });
      },
      mainnet_apps: function(callback) {
        // console.log('mainnet_apps');
        callback(null, []);
      },
      telos_nodes: function(callback) {
        // console.log('telos_nodes');
        var r = [];
        async.each(nodes.telos, function(node, cb) {
          var hb = heartbeats[node.name];
          var data = hb.data()
          r.push({
            data,
            name: node.name,
            response_time: hb.resTime()
          });
          cb();
        }, function(err) {
          // console.log('mainnet_nodes end');
          callback(err, r);
        });
      },
      telos_apps: function(callback) {
        // console.log('telos_apps');
        callback(null, []);
      },
      kylin_nodes: function(callback) {
        // console.log('telos_nodes');
        var r = [];
        async.each(nodes.kylin, function(node, cb) {
          var hb = heartbeats[node.name];
          var data = hb.data()
          r.push({
            data,
            name: node.name,
            response_time: hb.resTime()
          });
          cb();
        }, function(err) {
          // console.log('mainnet_nodes end');
          callback(err, r);
        });
      },
    }, function(error, result) {
      // console.log('result');
      if (error) {
        // console.log(error, 'error');
        socket.emit('error', error);
      } else {
        var obj = Object.assign({}, skt);
        obj.nodes.mainnet = _.sortBy(result.mainnet_nodes, ['name']);
        obj.nodes.telos = _.sortBy(result.telos_nodes, ['name']);
        obj.nodes.kylin = _.sortBy(result.kylin_nodes, ['name']);
        // console.log(obj, 'result');
        socket.emit('data', obj);
      }

    });

  });

  // force client disconnect from server
  socket.on('forceDisconnect', function() {
    socket.disconnect();
  })

  socket.on('disconnect', function() {
    // console.log('user disconnected: ' + socket.name);
  });
});

server.listen(3001, function() {
  console.log('Socket IO server listening on port 3001');
});