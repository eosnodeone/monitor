import React, { Component } from 'react'
import './css/App.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faServer } from '@fortawesome/free-solid-svg-icons'
import io from 'socket.io-client'
import _ from 'lodash'

library.add(faServer)

var socket;
if (process.env.NODE_ENV === 'production') {
  socket = io('/api');
} else {
  socket = io('http://localhost:3001');
}


class App extends Component {

  constructor() {

    super();

    this.state = {
      data: null,
      error: null
    }
  }

  componentDidMount() {

    // 서버로부터의 메시지가 수신되면
    socket.on('data', (data) => {
      this.setState({ data });
    });

    // 매 2초 간격으로 데이터 요청
    setInterval(() => {
      socket.emit("request data", 'hello');
    }, 2000);

  }

  componentWillUnmount() {
    
  }

  render() {
    const { data } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src="https://eosnodeone.io/resources/images/eosnodeone_logo.svg" className="App-logo" alt="logo" />
          <h1 className="App-title">NodeOne Infrastructure Monitor v0.1</h1>
        </header>
        <div className="App-content">
          {data ? data.platforms.map((platform, i) => (
            <div className="platform-section" key={`plfm-${i}`}>
              <h2 className="platform-title">{platform.name}</h2>
              <div className="monitor-group">
                <h3 className="group-title">- Nodes -</h3>
                <ul className="node-list">
                  {data.nodes[platform.key].map((item, i) => {
                    if (item.error || !item.data) {
                      return (
                        <li className="node-item" key={`mchn-${i}`} style={{color: 'red'}}><FontAwesomeIcon style={{float: 'left'}} icon="server" /> {item.name}</li>
                      );
                    } 
                    return (
                      <li className="node-item" key={`mchn-${i}`}><FontAwesomeIcon style={{float: 'left'}} icon="server" /> {item.name} #{item.data.head_block_num} ({item.response_time}ms)</li>
                    );
                  })}
                </ul>
              </div>
            </div>
          )) : undefined}
        </div>
      </div>
    );
  }
}

export default App;
