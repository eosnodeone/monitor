const fetch = require('node-fetch')


class Heartbeat {

	constructor({key, url, interval = 2000}) {
		this.key = key
		this.url = url
		this.interval = interval

		this._active = false
		this._state = -1
		this._lastResult = null
		this._resTime = -1
		this._timeout = 100
	}

	start() {
		this._active = true;

		this._getData();
	}

	data() {
		return this._lastResult
	}

	resTime() {
		return this._resTime
	}

	stop() {
		this._active = false
	}

	_setLastResult(data) {
		this._lastResult = data 
	}

	async _getData() {

		if (!this._active) return

		var response;
		var json;
		var err;
		var start = new Date()

		try {
	    const response = await fetch(this.url)
	    const json = await response.json()
	    this._setLastResult(json)
	    this._resTime = new Date() - start
	  } catch (error) {
	    console.log(error)
	    err = error
	  }

	  setTimeout(() => {
    	this._getData()
    }, this.interval)

	}

}

module.exports = Heartbeat