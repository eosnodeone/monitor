const express = require('express')
const Heartbeat = require('./heartbeat')

const app = express()

const h1 = new Heartbeat({
  key: 'h1',
  url: 'http://api.main-net.eosnodeone.io/v1/chain/get_info'
})

const h2 = new Heartbeat({
  key: 'h2',
  url: 'http://api.main-net.eosnodeone.io/v1/chain/get_info'
})

const h3 = new Heartbeat({
  key: 'h3',
  url: 'http://api.main-net.eosnodeone.io/v1/chain/get_info'
})


app.get('/', function (req, res) {
  const r = {
    h1: h1.data(),
    h2: h2.data(),
    h3: h3.data()
  }
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(r))
})


app.get('/start', function (req, res) {
  h1.start()
  h2.start()
  h3.start()
  res.setHeader('Content-Type', 'application/json')
  res.send(true)
})

app.listen(3000, function() {
  console.log('Test server listening on port 3000')
})